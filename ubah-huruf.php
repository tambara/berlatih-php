<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tes</title>
</head>
<body>

    <?php
        function ubah_huruf($string){
        //kode di sini
        $abjad = "abcdefghijklmnopqrstuvwxyz";
        $hasil = "";
        for($i=0; $i<strlen($string); $i++){
            $pos = strpos($abjad, $string[$i]);
            $hasil.= substr($abjad, $pos+1, 1);
        }
        return $hasil. "<br>";
    }

    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

    ?>

</body>
</html>